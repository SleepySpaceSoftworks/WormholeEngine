#pragma once
#include "wormhole/Core.hpp"

#include "wormhole/Memory.hpp"

namespace wh {
	template<typename Ty, std::size_t N>
	class RingBuffer {
	public:
		RingBuffer() {
			m_size = 0;
		}
		~RingBuffer() = default;
		
		std::size_t size() const noexcept { return m_size; }
		constexpr std::size_t capacity() const noexcept { return N; }

		[[nodiscard]] Ty pop();
		void push (const Ty& obj) noexcept;
		void push (Ty&& obj) noexcept;

		bool any() const noexcept { return m_size != 0; }
		bool full() const noexcept { return m_size == N; }
	private:
		std::atomic<std::size_t> m_size; //current number of objects being held
		std::atomic<std::size_t> m_begin; //index of the first object
		std::mutex m_mutex;
		Ty m_arr[N];
	};

	template<typename Ty, std::size_t N>
	[[nodiscard]] Ty RingBuffer<Ty, N>::pop() {
		if (m_size != 0) {
			std::lock_guard lk{ m_mutex };
			if (m_begin == N - 1) {
				m_begin = 0;
				return m_arr[N - 1];
			}
			else {
				++m_begin;
				return m_arr[m_begin - 1];
			}
		}
		else {
			throw std::out_of_range("wh::RingBuffer::pop() cannot return a value when empty.");
		}
	}

	template<typename Ty, std::size_t N>
	void RingBuffer<Ty, N>::push(const Ty& obj) noexcept {
		std::lock_guard lk{ m_mutex };
		++m_size;
		auto index = (m_begin + m_size) % N;
		m_arr[index] = obj;
	}

	template<typename Ty, std::size_t N>
	void RingBuffer<Ty, N>::push(Ty&& obj) noexcept {
		std::lock_guard lk{ m_mutex };
		++m_size;
		auto index = (m_begin + m_size) % N;
		m_arr[index] = std::move(obj);
	}
}
