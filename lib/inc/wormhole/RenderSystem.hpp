#pragma once
#include "wormhole/Core.hpp"

#include "wormhole/EngineSystem.hpp"
#include "wormhole/Window.hpp"

namespace wh {
	enum class BufferingMode : char { None, VSync, Adaptive, Triple };

	struct RenderSettings {
		float renderScale = 1.0f;
		BufferingMode bufferingMode = BufferingMode::VSync;
	};

	class RenderSystem final : public EngineSystem {
	public:
		RenderSystem() noexcept { }
		RenderSystem(const RenderSystem&) = delete;
		RenderSystem(RenderSystem&&) = delete;
		RenderSystem& operator=(const RenderSystem&) = delete;
		RenderSystem& operator=(RenderSystem&&) = delete;
		~RenderSystem() { }

		void startUp() override;
		void shutDown() override;

		void setWindow(Window* window);

		const RenderSettings& getSettings() const {
			return m_settings;
		}
	private:
		//helper class for loading in Vulkan functions
		class VulkanDispatcher {
		public:
			VulkanDispatcher();
			~VulkanDispatcher();

			VKAPI_ATTR VkResult VKAPI_CALL vkCreateInstance(const VkInstanceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkInstance* pInstance) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkEnumerateInstanceExtensionProperties(const char* pLayerName, uint32_t* pPropertyCount, VkExtensionProperties* pProperties) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkEnumerateInstanceLayerProperties(uint32_t* pPropertyCount, VkLayerProperties* pProperties) const;

			VKAPI_ATTR VkResult VKAPI_CALL vkCreateDevice(VkPhysicalDevice physicalDevice, const VkDeviceCreateInfo* pCreateInfo, 
				const VkAllocationCallbacks* pAllocator, VkDevice* pDevice) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkCreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
				const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) const;
			VKAPI_ATTR void VKAPI_CALL vkDestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, 
				const VkAllocationCallbacks* pAllocator) const;
			VKAPI_ATTR void VKAPI_CALL vkDestroyInstance(VkInstance instance, const VkAllocationCallbacks* pAllocator) const;
			VKAPI_ATTR void VKAPI_CALL vkDestroySurfaceKHR(VkInstance instance, VkSurfaceKHR surface, const VkAllocationCallbacks* pAllocator) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkEnumerateDeviceExtensionProperties(VkPhysicalDevice physicalDevice, 
				const char* pLayerName, uint32_t* pPropertyCount, VkExtensionProperties* pProperties) const;
			
			VKAPI_ATTR VkResult VKAPI_CALL vkEnumeratePhysicalDevices(VkInstance instance, uint32_t* pPhysicalDeviceCount, VkPhysicalDevice* pPhysicalDevices) const;
			VKAPI_ATTR void VKAPI_CALL vkGetPhysicalDeviceFeatures(VkPhysicalDevice physicalDevice, VkPhysicalDeviceFeatures* pFeatures) const;
			VKAPI_ATTR void VKAPI_CALL vkGetPhysicalDeviceProperties(VkPhysicalDevice physicalDevice, VkPhysicalDeviceProperties* pProperties) const;
			VKAPI_ATTR void VKAPI_CALL vkGetPhysicalDeviceQueueFamilyProperties(VkPhysicalDevice physicalDevice, 
				uint32_t* pQueueFamilyPropertyCount, VkQueueFamilyProperties* pQueueFamilyProperties) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkGetPhysicalDeviceSurfaceCapabilitiesKHR(VkPhysicalDevice physicalDevice, 
				VkSurfaceKHR surface, VkSurfaceCapabilitiesKHR* pSurfaceCapabilities) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkGetPhysicalDeviceSurfaceFormatsKHR(VkPhysicalDevice physicalDevice, 
				VkSurfaceKHR surface, uint32_t* pSurfaceFormatCount, VkSurfaceFormatKHR* pSurfaceFormats) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkGetPhysicalDeviceSurfacePresentModesKHR(VkPhysicalDevice physicalDevice, 
				VkSurfaceKHR surface, uint32_t* pPresentModeCount, VkPresentModeKHR* pPresentModes) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkGetPhysicalDeviceSurfaceSupportKHR(VkPhysicalDevice physicalDevice, uint32_t queueFamilyIndex,
				VkSurfaceKHR surface, VkBool32* pSupported) const;

			VKAPI_ATTR VkResult VKAPI_CALL vkAcquireNextImageKHR(VkDevice device, VkSwapchainKHR swapchain, uint64_t timeout, 
				VkSemaphore semaphrre, VkFence fence, uint32_t* pImageIndex) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkCreateSwapchainKHR(VkDevice device, const VkSwapchainCreateInfoKHR* pCreateInfo,
				const VkAllocationCallbacks* pAllocator, VkSwapchainKHR* swapchain) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkCreateImageView(VkDevice device, const VkImageViewCreateInfo* pCreateInfo, 
				const VkAllocationCallbacks* pAllocator, VkImageView* pView) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkDeviceWaitIdle(VkDevice device) const;
			VKAPI_ATTR void VKAPI_CALL vkDestroyDevice(VkDevice device, const VkAllocationCallbacks* pAllocator) const;
			VKAPI_ATTR void VKAPI_CALL vkDestroyImageView(VkDevice device, VkImageView imageView, const VkAllocationCallbacks* pAllocator) const;
			VKAPI_ATTR void VKAPI_CALL vkDestroySwapchainKHR(VkDevice device, VkSwapchainKHR swapchain, const VkAllocationCallbacks* allocator) const;
			VKAPI_ATTR void VKAPI_CALL vkGetDeviceQueue(VkDevice device, uint32_t queueFamilyIndex, uint32_t queueIndex, VkQueue* pQueue) const;
			VKAPI_ATTR VkResult VKAPI_CALL vkGetSwapchainImagesKHR(VkDevice device, VkSwapchainKHR swapchain, uint32_t* pSwapchainImageCount, VkImage* pSwapchainImages) const;
		private:
		#if defined _WINDOWS
			char const * const s_vulkanLibName = "vulkan-1.dll";
		#elif defined __linux__
			char const * const s_vulkanLibName = "libvulkan.so.1";
		#endif

			SharedLibrary m_vulkanLibrary{ nullptr };

			mutable PFN_vkGetInstanceProcAddr m_getInstanceProcAddr{ nullptr };
			
			// global-level functions
			mutable PFN_vkCreateInstance m_createInstance{ nullptr };
			mutable PFN_vkEnumerateInstanceExtensionProperties m_enumerateInstanceExtensionProperties{ nullptr };
			mutable PFN_vkEnumerateInstanceLayerProperties m_enumerateInstanceLayerProperties{ nullptr };

			// instance-level functions
			mutable PFN_vkCreateDevice m_createDevice{ nullptr };
			mutable PFN_vkCreateDebugUtilsMessengerEXT m_createDebugUtilsMessengerEXT{ nullptr };
			mutable PFN_vkDestroyDebugUtilsMessengerEXT m_destroyDebugUtilsMessengerEXT{ nullptr };
			mutable PFN_vkDestroyInstance m_destroyInstance{ nullptr };
			mutable PFN_vkDestroySurfaceKHR m_destroySurfaceKHR{ nullptr };
			mutable PFN_vkEnumerateDeviceExtensionProperties m_enumerateDeviceExtensionProperties{ nullptr };
			mutable PFN_vkEnumeratePhysicalDevices m_enumeratePhysicalDevices{ nullptr };
			mutable PFN_vkGetDeviceProcAddr m_getDeviceProcAddr{ nullptr };
			mutable PFN_vkGetPhysicalDeviceFeatures m_getPhysicalDeviceFeatures{ nullptr };
			mutable PFN_vkGetPhysicalDeviceProperties m_getPhysicalDeviceProperties{ nullptr };
			mutable PFN_vkGetPhysicalDeviceQueueFamilyProperties m_getPhysicalDeviceQueueFamilyProperties{ nullptr };
			mutable PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR m_getPhysicalDeviceSurfaceCapabilitiesKHR{ nullptr };
			mutable PFN_vkGetPhysicalDeviceSurfaceFormatsKHR m_getPhysicalDeviceSurfaceFormatsKHR{ nullptr };
			mutable PFN_vkGetPhysicalDeviceSurfacePresentModesKHR m_getPhysicalDeviceSurfacePresentModesKHR{ nullptr };
			mutable PFN_vkGetPhysicalDeviceSurfaceSupportKHR m_getPhysicalDeviceSurfaceSupportKHR{ nullptr };

			// device-level functions
			mutable PFN_vkAcquireNextImageKHR m_acquireNextImageKHR{ nullptr };
			mutable PFN_vkCreateSwapchainKHR m_createSwapchainKHR{ nullptr };
			mutable PFN_vkCreateImageView m_createImageView{ nullptr };
			mutable PFN_vkDeviceWaitIdle m_deviceWaitIdle{ nullptr };
			mutable PFN_vkDestroyDevice m_destroyDevice{ nullptr };
			mutable PFN_vkDestroyImageView m_destroyImageView{ nullptr };
			mutable PFN_vkDestroySwapchainKHR m_destroySwapchainKHR{ nullptr };
			mutable PFN_vkGetDeviceQueue m_getDeviceQueue{ nullptr };
			mutable PFN_vkGetSwapchainImagesKHR m_getSwapchainImagesKHR{ nullptr };
		};

		struct QueueFamilyIndices {
			int computeFamily = -1;
			int presentFamily = -1;
			int graphicsFamily = -1;

			bool isComplete() {
				return computeFamily >= 0 && graphicsFamily >= 0 && presentFamily >= 0;
			}
		};

		struct RenderWindow {
			Window* window{ nullptr };

			vk::SurfaceKHR surface{ nullptr };

			vk::SwapchainKHR swapchain{ nullptr };
			std::vector<vk::Image> swapchainImages;
			std::vector<vk::ImageView> swapChainImageViews;
			vk::Format swapchainImageFormat;
		};

		void createInstance();
		bool checkValidationLayerSupport();
		std::vector<const char*> getRequiredExtensions();
		bool validateExtensions(const std::vector<const char*>& extensions);

		void setUpDebugMessenger();
		static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
			VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
			VkDebugUtilsMessageTypeFlagsEXT messageType,
			const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
			void* pUserData);

		void pickPhysicalDevice();
		int rateDeviceSuitability(const vk::PhysicalDevice& device);
		bool checkDeviceExtensionSupport(const vk::PhysicalDevice& device);

		void createLogicalDevice();
		QueueFamilyIndices findQueueFamilies(const vk::PhysicalDevice& device);

		void createRenderContext();
		void createSurface();
		
		void createSwapchain();
		uint32_t chooseNumImages(const vk::SurfaceCapabilitiesKHR& surfaceCapabilities);
		std::pair<vk::Format, vk::ColorSpaceKHR> chooseSurfaceFormat();
		vk::PresentModeKHR choosePresentMode();

		void recreateSwapchain();

		void createImageViews();

		void destroyRenderContext();
		void destroySwapchain();

		const std::vector<const char*> m_validationLayers = {
			"VK_LAYER_KHRONOS_validation"
		};

		const std::vector<const char*> m_deviceExtensions = {
			VK_KHR_SWAPCHAIN_EXTENSION_NAME
		};

		VulkanDispatcher m_dispatcher;
		
		RenderSettings m_settings;

		vk::Instance m_instance{ nullptr };
		vk::DebugUtilsMessengerEXT m_debugMessenger{ nullptr };
		vk::PhysicalDevice m_physicalDevice{ nullptr };
		vk::Device m_device{ nullptr };

		vk::Queue m_computeQueue{ nullptr };
		vk::Queue m_graphicsQueue{ nullptr };
		vk::Queue m_presentQueue{ nullptr };

		RenderWindow m_renderWindow;

	#ifdef NDEBUG
		static constexpr bool m_enableValidationLayers = false;
	#else
		static constexpr bool m_enableValidationLayers = true;
	#endif
	};

	struct TextureImage {

	};
}
