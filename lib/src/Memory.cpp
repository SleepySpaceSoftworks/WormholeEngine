#include "wormhole/Memory.hpp"

namespace wh {
	Stack::Stack(size_t size) : m_data{ std::malloc(size) }, m_capacity{ size } {
		auto node = getBlock(0);
		node->size = size;
		node->next = 0;
	}

	Stack::~Stack() {
		std::lock_guard lk(m_dataMutex);
		std::free(m_data);
		m_data = nullptr;
	}

	void* Stack::allocate(const size_t size, const size_t alignment) {
		//make sure alignment is valid 
		ASSERT(1 <= alignment);
		ASSERT(alignment <= 128);
		ASSERT((alignment & (alignment - 1)) == 0); //check for a power of two

		//determine total amount of bytes we need to allocate
		size_t expandedSize = size + alignment;

		uintptr_t rawAddress = reinterpret_cast<uintptr_t>(allocate(expandedSize));

		size_t mask = alignment - 1; //AND mask
		uintptr_t misalignment = (rawAddress & mask);
		ptrdiff_t adjustment = alignment - misalignment;

		if (adjustment != 0 && reinterpret_cast<void*>(rawAddress) != m_data) {
			uint8_t* alignedAddress = reinterpret_cast<uint8_t*>(rawAddress) + adjustment;
			alignedAddress[-1] = static_cast<uint8_t>(adjustment);

			return reinterpret_cast<void*>(alignedAddress);
		}
		else {
			return reinterpret_cast<void*>(rawAddress);
		}
	}

	void* Stack::allocate(const size_t size) {
		ASSERT(size >= sizeof(FreeBlockHead));

		if (m_capacity - m_size < size) {
			throw std::bad_alloc();
		}
	
		std::lock_guard lk(m_dataMutex);

		//find the first free node that can fit the request
		FreeBlockHead* previousBlock = nullptr;
		FreeBlockHead* currentBlock = getBlock(m_freeBlockOffset);

		while (currentBlock->next != 0 && currentBlock->size < size) {
			previousBlock = currentBlock;
			currentBlock = getBlock(currentBlock->next);
		}

		if (currentBlock->size < size) {
			throw std::bad_alloc();
		}

		//if the block is the exact size we need
		if (currentBlock->size == size) {
			//if currentBlock is not the last block
			if (currentBlock->next != 0) {
				//if currentBlock is the first block
				if (previousBlock == nullptr) {
					if (currentBlock->next != 0) {
						m_freeBlockOffset = currentBlock->next;
					}
				}
				else {
					previousBlock->next = currentBlock->next;
				}
			}
			//if currentBlock is the last block
			else {
				auto newBlock = getBlock(getBlockOffset(currentBlock) + size);

				newBlock->size = currentBlock->size - size;
				newBlock->next = 0;

				if (previousBlock == nullptr) {
					m_freeBlockOffset = getBlockOffset(newBlock);
				}
				else {
					previousBlock->next = getBlockOffset(newBlock);
				}
			}
		}
		//the block size is bigger than what we need
		else { 
			//make a new sub-block
			auto newBlock = getBlock(getBlockOffset(currentBlock) + size);

			newBlock->size = currentBlock->size - size;
			newBlock->next = currentBlock->next;

			if (previousBlock == nullptr) {
				m_freeBlockOffset = getBlockOffset(newBlock);
			}
			else {
				previousBlock->next = getBlockOffset(newBlock);
			}
		}
	#ifndef NDEBUG
		//zero out the data in debug mode
		memset(currentBlock, 0, size);
	#endif
		m_size += size;
		return reinterpret_cast<void*>(currentBlock);
	}

	void Stack::deallocate(void* p, const size_t size, const size_t alignment) {
		//debug check for proper alignment
		ASSERT((reinterpret_cast<size_t>(p) & (alignment - 1)) == 0);

		if (p != m_data) {
			uint8_t* alignedAddress = reinterpret_cast<uint8_t*>(p);
			ptrdiff_t adjustment = static_cast<ptrdiff_t>(alignedAddress[-1]);
			uint8_t* rawAddress = alignedAddress - adjustment;
			deallocate(rawAddress, size);
		}
		else {
			deallocate(p, size);
		}
	}

	void Stack::deallocate(void* p, const size_t size) {
		if (p == nullptr) {
			return;
		}

		//assert that the pointer is within the pool's data
		ASSERT(reinterpret_cast<Byte*>(m_data) <= reinterpret_cast<Byte*>(p));
		ASSERT(reinterpret_cast<Byte*>(p) <= reinterpret_cast<Byte*>(m_data) + m_capacity);
		
		if (m_size != m_capacity) {
			FreeBlockHead* currentBlock = getBlock(m_freeBlockOffset);

			//try to have currentBlock be the block just behind p
			//TODO: change static_cast to narrow_cast when available
			while (currentBlock->next != 0 && currentBlock->next < static_cast<size_t>(reinterpret_cast<Byte*>(p) - reinterpret_cast<Byte*>(m_data))) {
				currentBlock = getBlock(currentBlock->next);
			}

			//if currentBlock is in front of p
			if (p  < reinterpret_cast<Byte*>(currentBlock)) {
				FreeBlockHead* newBlock = reinterpret_cast<FreeBlockHead*>(p);

				//if the blocks are back to back
				if (reinterpret_cast<Byte*>(p) + size == reinterpret_cast<Byte*>(currentBlock)) {
					newBlock->size = size + currentBlock->size;
					newBlock->next = currentBlock->next;
				}
				else {
					newBlock->size = size;
					newBlock->next = getBlockOffset(currentBlock);
				}

				m_freeBlockOffset = getBlockOffset(newBlock);
			}
			//current block is behind p
			else {
				//if the blocks are back to back
				if (p == reinterpret_cast<Byte*>(currentBlock) + currentBlock->size) {
					currentBlock->size += size;
					if (currentBlock->next != 0) {
						auto nextBlock = getBlock(currentBlock->next);

						if (reinterpret_cast<Byte*>(currentBlock) + currentBlock->size == reinterpret_cast<Byte*>(nextBlock)) {
							currentBlock->size += nextBlock->size;
							currentBlock->next = nextBlock->next;
						}
					}
				}
				else {
					FreeBlockHead* newBlock = reinterpret_cast<FreeBlockHead*>(p);
					newBlock->size = size;
					newBlock->next = currentBlock->next;
					currentBlock->next = getBlockOffset(newBlock);
				}
			}
		}
		else {
			FreeBlockHead* newBlock = reinterpret_cast<FreeBlockHead*>(p);
			newBlock->size = size;
			newBlock->next = 0;
			m_freeBlockOffset = getBlockOffset(newBlock);
		}

		m_size -= size;
	}
}
