#include <iostream>

#include "Wormhole.hpp"

class Game : public wh::Application {
public:
	Game() { }
	~Game() { }

	void run() {
		wh::Window window(800, 600, 60, wh::WindowMode::Windowed, "Window");
		wh::g_renderSystem->setWindow(&window);
		printf("Game is running!\n");

		wh::g_renderSystem->setWindow(nullptr);
	}
};

wh::Application* createApplication() {
	return new Game();
}
