#pragma once
#include <wormhole/Core.hpp>

#include <wormhole/RenderSystem.hpp>
#include <wormhole/ResourceSystem.hpp>
#include <wormhole/Application.hpp>

namespace wh {
	ResourceSystem* g_resourceSystem;
	RenderSystem* g_renderSystem;
}

extern wh::Application* createApplication();

int main(int argc, char * const argv[]) {
    wh::g_resourceSystem = new wh::ResourceSystem;
    wh::g_renderSystem = new wh::RenderSystem;

    wh::g_resourceSystem->startUp();
    wh::g_renderSystem->startUp();

    wh::Application* app = createApplication();
    app->run();
    delete app;

    wh::g_renderSystem->shutDown();
    wh::g_resourceSystem->shutDown();

    delete wh::g_renderSystem;
    delete wh::g_resourceSystem;
}