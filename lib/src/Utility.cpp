#include <cstring>

#include "wormhole/Utility.hpp"

namespace wh {
	UID hash(const std::string& string) {
		std::hash<std::string> hash;
		return hash(string);
	}

	Pool<char> String::s_allocator(WH_STRING_STACK_SIZE);

	String::String() : m_size(0) { }

	String::String(const String& other) {
		this->m_size = other.m_size;

		if(other.is_short()) { 
			std::memcpy(this->m_short, other.m_short, short_len);
		}
		else {
			this->m_long.data = s_allocator.allocate(this->m_size);
			this->m_long.capacity = this->m_size;
			std::memcpy(this->m_long.data, other.m_long.data, this->m_long.capacity);
		}
	}

	String::String(String&& other) {
		this->m_size = other.m_size;
		this->m_long = other.m_long;

		other.m_size = 0;
		other.m_long.data = nullptr;
		other.m_long.capacity = 0;
	}

	String::String(const char* str) {
		this->m_size = std::strlen(str) + 1;

		if(this->is_short()) {
			memcpy(this->m_short, str, this->m_size);
		}
		else {
			this->m_long.data = s_allocator.allocate(this->m_size);
			this->m_long.capacity = this->m_size;
			std::memcpy(this->m_long.data, str, this->m_long.capacity);
		}
	}

	String::String(const std::string& str) {
		this->m_size = str.size();
		
		if(this->is_short()) {
			std::memcpy(this->m_short, str.data(), this->m_size);
		}
		else {
			this->m_long.data = s_allocator.allocate(this->m_size);
			this->m_long.capacity = this->m_size;
			std::memcpy(this->m_long.data, str.data(), this->m_long.capacity);
		}
	}

	String& String::operator=(const String& rhs) {
		if(this != &rhs) {
			if(!this->is_short()) {
				s_allocator.deallocate(this->m_long.data, this->m_long.capacity);
			}

			this->m_size = rhs.m_size;

			if(rhs.is_short()) {
				std::memcpy(this->m_short, rhs.m_short, short_len);
			}
			else {
				this->m_long.data = s_allocator.allocate(rhs.m_size);
				this->m_long.capacity = this->m_size;
				std::memcpy(this->m_long.data, rhs.m_long.data, this->m_long.capacity);
			}
		}

		return *this;
	}

	String& String::operator=(String&& rhs) {
		if(!this->is_short()) {
			s_allocator.deallocate(this->m_long.data, this->m_long.capacity);
		}

		this->m_size = rhs.m_size;
		this->m_long = rhs.m_long;

		rhs.m_size = 0;
		rhs.m_long.data = nullptr;
		rhs.m_long.capacity = 0;
		
		return *this;
	}

	String& String::operator=(const char* rhs) {
		if(!this->is_short()) {
			s_allocator.deallocate(this->m_long.data, this->m_long.capacity);
		}

		this->m_size = std::strlen(rhs) + 1;

		if(this->is_short()) {
			memcpy(this->m_short, rhs, this->m_size);
		}
		else {
			this->m_long.data = s_allocator.allocate(this->m_size);
			this->m_long.capacity = this->m_size;
			std::memcpy(this->m_long.data, rhs, this->m_long.capacity);
		}

		return *this;
	}

	String& String::operator=(const std::string& rhs) {
		if(!this->is_short()) {
			s_allocator.deallocate(this->m_long.data, this->m_long.capacity);
		}

		this->m_size = rhs.size();
		
		if(this->is_short()) {
			std::memcpy(this->m_short, rhs.data(), this->m_size);
		}
		else {
			this->m_long.data = s_allocator.allocate(this->m_size);
			this->m_long.capacity = this->m_size;
			std::memcpy(this->m_long.data, rhs.data(), this->m_long.capacity);
		}

		return *this;
	}

	String::operator std::string_view() {
		if(is_short()) {
			return { m_short, m_size };
		}
		else {
			return { m_long.data, m_size };
		}
	}

	bool operator==(const String& lhs, const String& rhs) {
		return strcmp(lhs.c_str(), rhs.c_str()) == 0;
	}

	bool operator!=(const String& lhs, const String& rhs) {
		return !(lhs == rhs);
	}

	size_t String::size() const {
		return m_size;
	}

	size_t String::capacity() const {
		if(is_short()) {
			return short_len;
		}
		else {
			return m_long.capacity;
		}
	}

	const char* String::c_str() const {
		if(is_short()) {
			return m_short;
		}
		else {
			return m_long.data;
		}
	}

	String::~String() {
		if(m_size > short_len) {
			s_allocator.deallocate(m_long.data, m_long.capacity);
		}
	}

	std::ostream& operator<<(std::ostream& os, const String& str) {
		return os << str.c_str();
	}
}
