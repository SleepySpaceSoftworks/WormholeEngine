#pragma once
#include "wormhole/Core.hpp"

#include "wormhole/Memory.hpp"

namespace wh {
	[[nodiscard]] UID hash(const std::string& string);

// the amount of memory allocated for strings across the entire program
#ifndef WH_STRING_STACK_SIZE
#define WH_STRING_STACK_SIZE 50 KB 
#endif 

	class String {
		public:
			String();
			String(const String& other);
			String(String&& other);
			String(const char* str);
			String(const std::string& str);
			~String();

			String& operator=(const String& rhs);
			String& operator=(String&& rhs);
			String& operator=(const char* rhs);
			String& operator=(const std::string& rhs);

			operator std::string_view();

			friend bool operator==(const String& lhs, const String& rhs);
			friend bool operator!=(const String& lhs, const String& rhs);

			size_t size() const;
			size_t capacity() const;

			const char* c_str() const;
		private:
		static Pool<char> s_allocator;
			
		struct long_str {
			char* data{ nullptr };
			size_t capacity{ 0 };
		};

		// for short-string optimization
		union {
			long_str m_long;
			char m_short[sizeof(m_long)];
		};

		size_t m_size;

		static constexpr size_t short_len = sizeof(m_long);
		bool is_short() const { return m_size <= short_len; }
	};

	std::ostream& operator<<(std::ostream& os, const String& str);
}
