#include "wormhole/Platform.hpp"

//Windows implementations of functions
namespace wh {
	SharedLibrary openSharedLibrary(const char* path) {
		return LoadLibrary(path);
	}

	void* getProcAddress(SharedLibrary lib, const char* procName) {
		return GetProcAddress(lib, procName);
	}

	void closeSharedLibrary(SharedLibrary lib) {
		FreeLibrary(lib);
	}

	void* alignedAlloc(size_t size, size_t alignment) {
		return _aligned_malloc(size, alignment);
	}

	void alignedFree(void* p) {
		_aligned_free(p);
	}
}