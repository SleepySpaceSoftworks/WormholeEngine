#include "wormhole/Platform.hpp"

#include <cstdlib>

//Linux implementations of functions
namespace wh {
	SharedLibrary openSharedLibrary(const char* path) {
		return dlopen(path, RTLD_NOW);
	}

	void* getProcAddress(SharedLibrary lib, const char* procName) {
		return dlsym(lib, procName);
	}

	void closeSharedLibrary(SharedLibrary lib) {
		dlclose(lib);
	}

	void* alignedAlloc(size_t size, size_t alignment) {
		return std::aligned_alloc(alignment, size);
	}

	void alignedFree(void* p) {
		std::free(p);
	}
}
