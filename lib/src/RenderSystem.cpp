#include "wormhole/RenderSystem.hpp"

namespace wh {
	RenderSystem::VulkanDispatcher::VulkanDispatcher() {
		m_vulkanLibrary = openSharedLibrary(s_vulkanLibName);
		m_getInstanceProcAddr = (PFN_vkGetInstanceProcAddr) getProcAddress(m_vulkanLibrary, "vkGetInstanceProcAddr");

		// initialize global level functions
		m_createInstance = (PFN_vkCreateInstance) m_getInstanceProcAddr(nullptr, "vkCreateInstance");
		m_enumerateInstanceExtensionProperties = (PFN_vkEnumerateInstanceExtensionProperties) m_getInstanceProcAddr(nullptr, "vkEnumerateInstanceExtensionProperties");
		m_enumerateInstanceLayerProperties = (PFN_vkEnumerateInstanceLayerProperties) m_getInstanceProcAddr(nullptr, "vkEnumerateInstanceLayerProperties");
	}

	RenderSystem::VulkanDispatcher::~VulkanDispatcher() {
		closeSharedLibrary(m_vulkanLibrary);
		m_vulkanLibrary = nullptr;
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkCreateInstance(const VkInstanceCreateInfo* pCreateInfo,
		const VkAllocationCallbacks* pAllocator, VkInstance* pInstance) const {
		auto result = m_createInstance(pCreateInfo, pAllocator, pInstance);
		
		// initialize instance level functions
		m_createDevice = (PFN_vkCreateDevice) m_getInstanceProcAddr(*pInstance, "vkCreateDevice");
		m_createDebugUtilsMessengerEXT = (PFN_vkCreateDebugUtilsMessengerEXT) m_getInstanceProcAddr(*pInstance, "vkCreateDebugUtilsMessengerEXT");
		m_destroyDebugUtilsMessengerEXT = (PFN_vkDestroyDebugUtilsMessengerEXT) m_getInstanceProcAddr(*pInstance, "vkDestroyDebugUtilsMessengerEXT");
		m_destroyInstance = (PFN_vkDestroyInstance) m_getInstanceProcAddr(*pInstance, "vkDestroyInstance");
		m_destroySurfaceKHR = (PFN_vkDestroySurfaceKHR) m_getInstanceProcAddr(*pInstance, "vkDestroySurfaceKHR");
		m_enumerateDeviceExtensionProperties = (PFN_vkEnumerateDeviceExtensionProperties) m_getInstanceProcAddr(*pInstance, "vkEnumerateDeviceExtensionProperties");
		m_enumeratePhysicalDevices = (PFN_vkEnumeratePhysicalDevices) m_getInstanceProcAddr(*pInstance, "vkEnumeratePhysicalDevices");
		m_getDeviceProcAddr = (PFN_vkGetDeviceProcAddr) m_getInstanceProcAddr(*pInstance, "vkGetDeviceProcAddr");
		m_getPhysicalDeviceFeatures = (PFN_vkGetPhysicalDeviceFeatures) m_getInstanceProcAddr(*pInstance, "vkGetPhysicalDeviceFeatures");
		m_getPhysicalDeviceProperties = (PFN_vkGetPhysicalDeviceProperties) m_getInstanceProcAddr(*pInstance, "vkGetPhysicalDeviceProperties");
		m_getPhysicalDeviceQueueFamilyProperties = (PFN_vkGetPhysicalDeviceQueueFamilyProperties) m_getInstanceProcAddr(*pInstance, 
			"vkGetPhysicalDeviceQueueFamilyProperties");
		m_getPhysicalDeviceSurfaceCapabilitiesKHR = (PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR) m_getInstanceProcAddr(*pInstance, 
			"vkGetPhysicalDeviceSurfaceCapabilitiesKHR");
		m_getPhysicalDeviceSurfaceFormatsKHR = (PFN_vkGetPhysicalDeviceSurfaceFormatsKHR) m_getInstanceProcAddr(*pInstance, "vkGetPhysicalDeviceSurfaceFormatsKHR");
		m_getPhysicalDeviceSurfacePresentModesKHR = (PFN_vkGetPhysicalDeviceSurfacePresentModesKHR) m_getInstanceProcAddr(*pInstance, 
			"vkGetPhysicalDeviceSurfacePresentModesKHR");
		m_getPhysicalDeviceSurfaceSupportKHR = (PFN_vkGetPhysicalDeviceSurfaceSupportKHR) m_getInstanceProcAddr(*pInstance, "vkGetPhysicalDeviceSurfaceSupportKHR");

		return result;
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkEnumerateInstanceExtensionProperties(const char* pLayerName, 
		uint32_t* pPropertyCount, VkExtensionProperties* pProperties) const {
		return m_enumerateInstanceExtensionProperties(pLayerName, pPropertyCount, pProperties);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkEnumerateInstanceLayerProperties(uint32_t* pPropertyCount, VkLayerProperties* pProperties) const {
		return m_enumerateInstanceLayerProperties(pPropertyCount, pProperties);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkEnumeratePhysicalDevices(VkInstance instance, uint32_t* pPhysicalDeviceCount, VkPhysicalDevice* pPhysicalDevices) const {
		return m_enumeratePhysicalDevices(instance, pPhysicalDeviceCount, pPhysicalDevices);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkCreateDevice(VkPhysicalDevice physicalDevice, 
		const VkDeviceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDevice* pDevice) const {
		auto result = m_createDevice(physicalDevice, pCreateInfo, pAllocator, pDevice);

		// initialize device level functions
		m_acquireNextImageKHR = (PFN_vkAcquireNextImageKHR) m_getDeviceProcAddr(*pDevice, "vkAcquireNextImageKHR");
		m_createSwapchainKHR = (PFN_vkCreateSwapchainKHR) m_getDeviceProcAddr(*pDevice, "vkCreateSwapchainKHR");
		m_createImageView = (PFN_vkCreateImageView) m_getDeviceProcAddr(*pDevice, "vkCreateImageView");
		m_deviceWaitIdle = (PFN_vkDeviceWaitIdle) m_getDeviceProcAddr(*pDevice, "vkDeviceWaitIdle");
		m_destroyDevice = (PFN_vkDestroyDevice) m_getDeviceProcAddr(*pDevice, "vkDestroyDevice");
		m_destroyImageView = (PFN_vkDestroyImageView) m_getDeviceProcAddr(*pDevice, "vkDestroyImageView");
		m_destroySwapchainKHR = (PFN_vkDestroySwapchainKHR) m_getDeviceProcAddr(*pDevice, "vkDestroySwapchainKHR");
		m_getDeviceQueue = (PFN_vkGetDeviceQueue) m_getDeviceProcAddr(*pDevice, "vkGetDeviceQueue");
		m_getSwapchainImagesKHR = (PFN_vkGetSwapchainImagesKHR)  m_getDeviceProcAddr(*pDevice, "vkGetSwapchainImagesKHR");

		return result;
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkCreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
		const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) const {
		return m_createDebugUtilsMessengerEXT(instance, pCreateInfo, pAllocator, pDebugMessenger);
	}

	VKAPI_ATTR void VKAPI_CALL RenderSystem::VulkanDispatcher::vkDestroyDebugUtilsMessengerEXT(VkInstance instance, 
		VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) const {
		m_destroyDebugUtilsMessengerEXT(instance, debugMessenger, pAllocator);
	}

	VKAPI_ATTR void VKAPI_CALL RenderSystem::VulkanDispatcher::vkDestroyInstance(VkInstance instance, const VkAllocationCallbacks* pAllocator) const {
		m_destroyInstance(instance, pAllocator);

		// de-initialize instance level functions
		m_createDevice = nullptr;
		m_createDebugUtilsMessengerEXT = nullptr;
		m_destroyDebugUtilsMessengerEXT = nullptr;
		m_destroyInstance = nullptr;
		m_destroySurfaceKHR = nullptr;
		m_enumerateDeviceExtensionProperties = nullptr;
		m_enumeratePhysicalDevices = nullptr;
		m_getDeviceProcAddr = nullptr;
		m_getPhysicalDeviceFeatures = nullptr;
		m_getPhysicalDeviceProperties = nullptr;
		m_getPhysicalDeviceQueueFamilyProperties = nullptr;
		m_getPhysicalDeviceSurfaceCapabilitiesKHR = nullptr;
		m_getPhysicalDeviceSurfaceFormatsKHR = nullptr;
		m_getPhysicalDeviceSurfacePresentModesKHR = nullptr;
		m_getPhysicalDeviceSurfaceSupportKHR = nullptr;
	}

	VKAPI_ATTR void VKAPI_CALL RenderSystem::VulkanDispatcher::vkDestroySurfaceKHR(VkInstance instance, VkSurfaceKHR surface, const VkAllocationCallbacks* pAllocator) const {
		m_destroySurfaceKHR(instance, surface, pAllocator);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkEnumerateDeviceExtensionProperties(VkPhysicalDevice physicalDevice,
		const char* pLayerName, uint32_t* pPropertyCount, VkExtensionProperties* pProperties) const {
		return m_enumerateDeviceExtensionProperties(physicalDevice, pLayerName, pPropertyCount, pProperties);
	}

	VKAPI_ATTR void VKAPI_CALL RenderSystem::VulkanDispatcher::vkGetPhysicalDeviceFeatures(VkPhysicalDevice physicalDevice, VkPhysicalDeviceFeatures* pFeatures) const {
		m_getPhysicalDeviceFeatures(physicalDevice, pFeatures);
	}

	VKAPI_ATTR void VKAPI_CALL RenderSystem::VulkanDispatcher::vkGetPhysicalDeviceProperties(VkPhysicalDevice physicalDevice, VkPhysicalDeviceProperties* pProperties) const {
		m_getPhysicalDeviceProperties(physicalDevice, pProperties);
	}

	VKAPI_ATTR void VKAPI_CALL RenderSystem::VulkanDispatcher::vkGetPhysicalDeviceQueueFamilyProperties(VkPhysicalDevice physicalDevice, 
		uint32_t* pQueueFamilyPropertyCount, VkQueueFamilyProperties* pQueueFamilyProperties) const {
		m_getPhysicalDeviceQueueFamilyProperties(physicalDevice, pQueueFamilyPropertyCount, pQueueFamilyProperties);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkGetPhysicalDeviceSurfaceCapabilitiesKHR(VkPhysicalDevice physicalDevice,
		VkSurfaceKHR surface, VkSurfaceCapabilitiesKHR* pSurfaceCapabilities) const {
		return m_getPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, pSurfaceCapabilities);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkGetPhysicalDeviceSurfaceFormatsKHR(VkPhysicalDevice physicalDevice,
		VkSurfaceKHR surface, uint32_t* pSurfaceFormatCount, VkSurfaceFormatKHR* pSurfaceFormats) const {
		return m_getPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, pSurfaceFormatCount, pSurfaceFormats);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkGetPhysicalDeviceSurfacePresentModesKHR(VkPhysicalDevice physicalDevice,
		VkSurfaceKHR surface, uint32_t* pPresentModeCount, VkPresentModeKHR* pPresentModes) const {
		return m_getPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, pPresentModeCount, pPresentModes);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkGetPhysicalDeviceSurfaceSupportKHR(VkPhysicalDevice physicalDevice, uint32_t queueFamilyIndex,
		VkSurfaceKHR surface, VkBool32* pSupported) const {
		return m_getPhysicalDeviceSurfaceSupportKHR(physicalDevice, queueFamilyIndex, surface, pSupported);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkAcquireNextImageKHR(VkDevice device, VkSwapchainKHR swapchain, 
		uint64_t timeout, VkSemaphore semaphore, VkFence fence, uint32_t* pImageIndex) const {
		return m_acquireNextImageKHR(device, swapchain, timeout, semaphore, fence, pImageIndex);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkCreateSwapchainKHR(VkDevice device, const VkSwapchainCreateInfoKHR* pCreateInfo, 
		const VkAllocationCallbacks* pAllocator, VkSwapchainKHR* swapchain) const {
		return m_createSwapchainKHR(device, pCreateInfo, pAllocator, swapchain);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkCreateImageView(VkDevice device, const VkImageViewCreateInfo* pCreateInfo,
		const VkAllocationCallbacks* pAllocator, VkImageView* pView) const {
		return m_createImageView(device, pCreateInfo, pAllocator, pView);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkDeviceWaitIdle(VkDevice device) const {
		return m_deviceWaitIdle(device);	
	}

	VKAPI_ATTR void VKAPI_CALL RenderSystem::VulkanDispatcher::vkDestroyDevice(VkDevice device, const VkAllocationCallbacks* pAllocator) const {
		m_destroyDevice(device, pAllocator);

		// de-initialize device level functions
		m_acquireNextImageKHR = nullptr;
		m_createSwapchainKHR = nullptr;
		m_createImageView = nullptr;
		m_deviceWaitIdle = nullptr;
		m_destroyDevice = nullptr;
		m_destroyImageView = nullptr;
		m_destroySwapchainKHR = nullptr;
		m_getDeviceQueue = nullptr;
		m_getSwapchainImagesKHR = nullptr;
	}

	VKAPI_ATTR void VKAPI_CALL RenderSystem::VulkanDispatcher::vkDestroyImageView(VkDevice device, VkImageView imageView, const VkAllocationCallbacks* pAllocator) const {
		m_destroyImageView(device, imageView, pAllocator);
	}

	VKAPI_ATTR void VKAPI_CALL RenderSystem::VulkanDispatcher::vkDestroySwapchainKHR(VkDevice device, 
		VkSwapchainKHR swapchain, const VkAllocationCallbacks* allocator) const {
		m_destroySwapchainKHR(device, swapchain, allocator);
	}

	VKAPI_ATTR void VKAPI_CALL RenderSystem::VulkanDispatcher::vkGetDeviceQueue(VkDevice device, uint32_t queueFamilyIndex, uint32_t queueIndex, VkQueue* pQueue) const {
		m_getDeviceQueue(device, queueFamilyIndex, queueIndex, pQueue);
	}

	VKAPI_ATTR VkResult VKAPI_CALL RenderSystem::VulkanDispatcher::vkGetSwapchainImagesKHR(VkDevice device, 
		VkSwapchainKHR swapchain, uint32_t* pSwapchainImageCount, VkImage* pSwapchainImages) const {
		return m_getSwapchainImagesKHR(device, swapchain, pSwapchainImageCount, pSwapchainImages);
	}

	void RenderSystem::startUp() {
		glfwInit();
		createInstance();
		setUpDebugMessenger();
		pickPhysicalDevice();
		createLogicalDevice();
	}

	void RenderSystem::shutDown() {
		m_device.destroy(nullptr, m_dispatcher);

		if (m_enableValidationLayers) {
			m_instance.destroyDebugUtilsMessengerEXT(m_debugMessenger, nullptr, m_dispatcher);
		}

		m_instance.destroy(nullptr, m_dispatcher);
		glfwTerminate();
	}

	void RenderSystem::setWindow(Window* pWindow) {
		if (m_renderWindow.window != nullptr) {
			destroyRenderContext();
		}

		m_renderWindow.window = pWindow;

		if(m_renderWindow.window != nullptr) {
			createRenderContext();
		}
	}

	void RenderSystem::createInstance() {
		if (m_enableValidationLayers && !checkValidationLayerSupport()) {
			throw std::runtime_error("wh::RenderSystem: Validation layers requested, but not available!");
		}

		vk::ApplicationInfo appInfo("WormholeEngine", VK_MAKE_VERSION(1, 0, 0), "WormholeEngine", 
			VK_MAKE_VERSION(VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH), VK_API_VERSION_1_0);

		vk::InstanceCreateInfo createInfo;
		createInfo.pApplicationInfo = &appInfo;

		auto requiredExtensions = getRequiredExtensions();

		if (!validateExtensions(requiredExtensions)) {
			throw std::runtime_error("wh::RenderSystem: Could not find required extensions!");
		}

		createInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensions.size());
		createInfo.ppEnabledExtensionNames = requiredExtensions.data();

		if (m_enableValidationLayers) {
			createInfo.enabledLayerCount = static_cast<uint32_t>(m_validationLayers.size());
			createInfo.ppEnabledLayerNames = m_validationLayers.data();
		}
		else {
			createInfo.enabledLayerCount = 0;
		}

		m_instance = vk::createInstance(createInfo, nullptr, m_dispatcher);
	}

	bool RenderSystem::checkValidationLayerSupport() {
		std::vector<vk::LayerProperties> availableLayers = vk::enumerateInstanceLayerProperties(m_dispatcher);

		for (const char* layerName : m_validationLayers) {
			bool layerFound = false;

			for (const auto& layerProperties : availableLayers) {
				if (strcmp(layerName, layerProperties.layerName) == 0) {
					layerFound = true;
					break;
				}
			}

			if (!layerFound) {
				return false;
			}
		}

		return true;
	}

	std::vector<const char*> RenderSystem::getRequiredExtensions() {
		std::vector<const char*> requiredExtensions;

		unsigned int glfwExtensionsCount = 0;
		const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);

		for (unsigned int i = 0; i < glfwExtensionsCount; i++) {
			requiredExtensions.push_back(glfwExtensions[i]);
		}

		if (m_enableValidationLayers) {
			requiredExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		}

		return requiredExtensions;
	}

	bool RenderSystem::validateExtensions(const std::vector<const char*>& extensions) {
		std::vector<vk::ExtensionProperties> availableExtensions = vk::enumerateInstanceExtensionProperties(nullptr, m_dispatcher);
		

		for (const auto& extension : extensions) {
			bool found = false;
			for (const auto& availExtension : availableExtensions) {
				if (std::strcmp(extension, availExtension.extensionName) == 0) {
					found = true;
					break;
				}
			}

			if (!found) {
				return false;
			}
		}

		return true;
	}

	void RenderSystem::setUpDebugMessenger() {
		if (!m_enableValidationLayers) return;

		vk::DebugUtilsMessengerCreateInfoEXT createInfo = {};
		createInfo.messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT::eError |
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning;

		createInfo.messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
			vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance |
			vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation;

		createInfo.pfnUserCallback = debugCallback;

		m_debugMessenger = m_instance.createDebugUtilsMessengerEXT(createInfo, nullptr, m_dispatcher);
	}

	VKAPI_ATTR VkBool32 VKAPI_CALL RenderSystem::debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT,
		VkDebugUtilsMessageTypeFlagsEXT,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void*) {
		fprintf(stderr, "Vulkan: %s\n", pCallbackData->pMessage);
		return VK_FALSE;
	}

	void RenderSystem::pickPhysicalDevice() {
		std::vector<vk::PhysicalDevice> devices = m_instance.enumeratePhysicalDevices(m_dispatcher);
		std::multimap<int, vk::PhysicalDevice> candidates;

		for (const auto& device : devices) {
			int score = rateDeviceSuitability(device);
			candidates.insert(std::make_pair(score, device));
		}

		if (candidates.rbegin()->first > 0) {
			m_physicalDevice = candidates.rbegin()->second;
		}

		if (!m_physicalDevice) {
			throw std::runtime_error("wh::RenderSystem: Was not able to find a suitable device.");
		}
	}

	int RenderSystem::rateDeviceSuitability(const vk::PhysicalDevice& device) {
		int score = 0;

		vk::PhysicalDeviceProperties deviceProperties = device.getProperties(m_dispatcher);
		vk::PhysicalDeviceFeatures deviceFeatures = device.getFeatures(m_dispatcher);

		if (deviceProperties.deviceType == vk::PhysicalDeviceType::eDiscreteGpu) {
			// discrete GPUs are preferred over integrated gpus
			score += 1000;
		}

		score += deviceProperties.limits.maxImageDimension2D;
		score += deviceProperties.limits.maxImageDimension3D;

		auto queueFamilyIndices = findQueueFamilies(device);

		if (!deviceFeatures.geometryShader || !deviceFeatures.samplerAnisotropy || !checkDeviceExtensionSupport(device) || !queueFamilyIndices.isComplete()) {
			// if any of the features we need are not supported, it's not suitable at all
			return 0;
		}

		return score;
	}

	bool RenderSystem::checkDeviceExtensionSupport(const vk::PhysicalDevice& device) {
		std::vector<vk::ExtensionProperties> availableExtensions = device.enumerateDeviceExtensionProperties(nullptr, m_dispatcher);

		std::set<std::string> requiredExtensions(m_deviceExtensions.begin(), m_deviceExtensions.end());

		for (const auto& extension : availableExtensions) {
			requiredExtensions.erase(extension.extensionName);
		}

		return requiredExtensions.empty();
	}

	void RenderSystem::createLogicalDevice() {
		QueueFamilyIndices indices = findQueueFamilies(m_physicalDevice);

		std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
		std::set<int> uniqueQueueFamilies = { indices.computeFamily, indices.graphicsFamily, indices.presentFamily };

		float queuePriority = 1.0f;

		for (auto queueFamily : uniqueQueueFamilies) {
			queueCreateInfos.emplace_back(vk::DeviceQueueCreateFlags(), queueFamily, 1, &queuePriority);
		}

		vk::PhysicalDeviceFeatures deviceFeatures = m_physicalDevice.getFeatures();

		vk::DeviceCreateInfo createInfo{ };
		createInfo.pQueueCreateInfos = queueCreateInfos.data();
		createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());

		createInfo.pEnabledFeatures = &deviceFeatures;

		createInfo.enabledExtensionCount = static_cast<uint32_t>(m_deviceExtensions.size());
		createInfo.ppEnabledExtensionNames = m_deviceExtensions.data();

		if (m_physicalDevice.createDevice(&createInfo, nullptr, &m_device, m_dispatcher) != vk::Result::eSuccess) {
			throw std::runtime_error("wh::RenderSystem: Failed to create logical device.");
		}

		m_computeQueue = m_device.getQueue(indices.computeFamily, 0, m_dispatcher);
		m_graphicsQueue = m_device.getQueue(indices.graphicsFamily, 0, m_dispatcher);
		m_presentQueue = m_device.getQueue(indices.presentFamily, 0, m_dispatcher);
	}

	RenderSystem::QueueFamilyIndices RenderSystem::findQueueFamilies(const vk::PhysicalDevice& device) {
		QueueFamilyIndices indices;

		std::vector<vk::QueueFamilyProperties> queueFamilies = device.getQueueFamilyProperties(m_dispatcher);

		short int i = 0;
		for (const auto& queueFamily : queueFamilies) {
			if (queueFamily.queueCount > 0) {
				// check compute support
				if (queueFamily.queueFlags & vk::QueueFlagBits::eCompute) {
					indices.computeFamily = i;
				}

				// check present support
				if (glfwGetPhysicalDevicePresentationSupport(m_instance, device, i)) {
					indices.presentFamily = i;
				}

				// check graphics support
				if (queueFamily.queueFlags & vk::QueueFlagBits::eGraphics) {
					indices.graphicsFamily = i;
				}

				if (indices.isComplete()) {
					break;
				}
			}

			i++;
		}

		return indices;
	}

	void RenderSystem::createRenderContext() {
		createSurface();
		createSwapchain();
		createImageViews();
	}

	void RenderSystem::createSurface() {
		VkSurfaceKHR rawSurface;

		if (glfwCreateWindowSurface(m_instance, m_renderWindow.window->handle(), nullptr, &rawSurface) != VK_SUCCESS) {
			throw std::runtime_error("wh::RenderSystem: Could not create window surface!");
		}

		// ensure we can actually use the present queue to present to this surface
		auto queueFamilies = findQueueFamilies(m_physicalDevice);
		if (!m_physicalDevice.getSurfaceSupportKHR(queueFamilies.presentFamily, rawSurface, m_dispatcher)) {
			throw std::runtime_error("wh::RenderSystem: Could not get support for window surface!");
		}

		m_renderWindow.surface = rawSurface;
	}

	void RenderSystem::createSwapchain() {
		vk::SwapchainCreateInfoKHR createInfo;

		vk::SurfaceCapabilitiesKHR surfaceCapabilities = m_physicalDevice.getSurfaceCapabilitiesKHR(m_renderWindow.surface, m_dispatcher);
		createInfo.minImageCount = chooseNumImages(surfaceCapabilities);
		createInfo.imageExtent = surfaceCapabilities.currentExtent;

		createInfo.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;

		auto[format, colorSpace] = chooseSurfaceFormat();
		m_renderWindow.swapchainImageFormat = format;
		createInfo.imageFormat = format;
		createInfo.imageColorSpace = colorSpace;

		createInfo.surface = m_renderWindow.surface;
		createInfo.imageArrayLayers = 1;
		createInfo.imageSharingMode = vk::SharingMode::eExclusive;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
		createInfo.preTransform = vk::SurfaceTransformFlagBitsKHR::eIdentity;
		createInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
		createInfo.presentMode = choosePresentMode();
		createInfo.clipped = VK_FALSE;
		createInfo.oldSwapchain = nullptr;

		m_renderWindow.swapchain = m_device.createSwapchainKHR(createInfo, nullptr, m_dispatcher);

		if (!m_renderWindow.swapchain) {
			throw std::runtime_error("wh::RenderSystem: Could not create swapchain!");
		}

		m_renderWindow.swapchainImages = m_device.getSwapchainImagesKHR(m_renderWindow.swapchain, m_dispatcher);
	}

	uint32_t RenderSystem::chooseNumImages(const vk::SurfaceCapabilitiesKHR& surfaceCapabilities) {
		uint32_t numImages = surfaceCapabilities.minImageCount + 1;

		if (surfaceCapabilities.maxImageCount > 0 && numImages > surfaceCapabilities.maxImageCount) {
			numImages = surfaceCapabilities.maxImageCount;
		}

		return numImages;
	}

	std::pair<vk::Format, vk::ColorSpaceKHR> RenderSystem::chooseSurfaceFormat() {
		vk::Format desiredFormat = vk::Format::eR8G8B8A8Unorm;
		vk::ColorSpaceKHR desiredColorSpace = vk::ColorSpaceKHR::eSrgbNonlinear;

		auto supportedFormats = m_physicalDevice.getSurfaceFormatsKHR(m_renderWindow.surface, m_dispatcher);

		// an undefined format means that we can use any format and color-space
		if (supportedFormats.size() == 1 && supportedFormats[0].format == vk::Format::eUndefined) {
			return { desiredFormat, desiredColorSpace };
		}
		else {
			for (auto& format : supportedFormats) {
				if (format.format == desiredFormat && format.colorSpace == desiredColorSpace) {
					return { desiredFormat, desiredColorSpace };
				}
			}

			// if we cannot get our desired format and color-space, try to get just the format
			for (auto& format : supportedFormats) {
				if (format.format == desiredFormat) {
					return { desiredFormat, format.colorSpace };
				}
			}

			// if nothing can be found, just take the first pair
			return { supportedFormats[0].format,  supportedFormats[0].colorSpace };
		}
	}

	vk::PresentModeKHR RenderSystem::choosePresentMode() {
		auto presentModes = m_physicalDevice.getSurfacePresentModesKHR(m_renderWindow.surface, m_dispatcher);

		for (auto& presentMode : presentModes) {
			if (m_settings.bufferingMode == BufferingMode::None && presentMode == vk::PresentModeKHR::eImmediate) {
				return presentMode;
			}
			else if (m_settings.bufferingMode == BufferingMode::VSync && presentMode == vk::PresentModeKHR::eFifo) {
				return presentMode;
			}
			else if (m_settings.bufferingMode == BufferingMode::Adaptive && presentMode == vk::PresentModeKHR::eFifoRelaxed) {
				return presentMode;
			}
			else if (m_settings.bufferingMode == BufferingMode::Triple && presentMode == vk::PresentModeKHR::eMailbox) {
				return presentMode;
			}
		}

		// our requested present mode was not found, and VSync/FIFO is required by the Vulkan standard so we default to it
		m_settings.bufferingMode = BufferingMode::VSync;
		return vk::PresentModeKHR::eFifo;
	}

	void RenderSystem::recreateSwapchain() {
		m_device.waitIdle(m_dispatcher);
		destroySwapchain();
		createSwapchain();
	}

	void RenderSystem::createImageViews() {
		m_renderWindow.swapChainImageViews.resize(m_renderWindow.swapchainImages.size());

		for (size_t i = 0; i < m_renderWindow.swapChainImageViews.size(); i++) {
			vk::ImageViewCreateInfo createInfo;
			
			createInfo.image = m_renderWindow.swapchainImages[i];
			
			createInfo.viewType = vk::ImageViewType::e2D;
			createInfo.format = m_renderWindow.swapchainImageFormat;

			createInfo.components.r = vk::ComponentSwizzle::eIdentity;
			createInfo.components.g = vk::ComponentSwizzle::eIdentity;
			createInfo.components.b = vk::ComponentSwizzle::eIdentity;
			createInfo.components.a = vk::ComponentSwizzle::eIdentity;

			createInfo.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			createInfo.subresourceRange.baseMipLevel = 0;
			createInfo.subresourceRange.levelCount = 1;
			createInfo.subresourceRange.baseArrayLayer = 0;
			createInfo.subresourceRange.layerCount = 1;

			m_renderWindow.swapChainImageViews[i] = m_device.createImageView(createInfo, nullptr, m_dispatcher);
		}
	}

	void RenderSystem::destroyRenderContext() {
		destroySwapchain();
		m_instance.destroySurfaceKHR(m_renderWindow.surface, nullptr, m_dispatcher);
		m_renderWindow.window = nullptr;
	}

	void RenderSystem::destroySwapchain() {
		for (size_t i = 0; i < m_renderWindow.swapChainImageViews.size(); i++) {
			m_device.destroyImageView(m_renderWindow.swapChainImageViews[i], nullptr, m_dispatcher);
		}

		m_device.destroySwapchainKHR(m_renderWindow.swapchain, nullptr, m_dispatcher);
	}
}
