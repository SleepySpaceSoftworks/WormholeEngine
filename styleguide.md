# __Style Guide__

## **1:** Always use enum class instead of plain enums

Doing this prevents name clashes.

## **2:** Only engine systems may have init and cleanup functions; everything else needs to be initialized in its constructor

TODO: add reason

## **3:**  Class, struct, and enum names must be written in UpperCamelCase, and everything else must be written in lowerCamelCase

For the sake of consistency and readability

## **4:** All-non public data member names must be preceded by an m and a underscore (i.e. m_privateData)

To make a clear distinction between implementations and interfaces

## **5:** Any repeating bits of code should be substituted by a helper function

To make it easier to maintain and improve code and increase readability of the code

## **6:** Keep functions as short as possible

To keep functions readable and able to be reasoned about

## **7:** Limit friendship as much as possible

It breaks encapsutlation

## **8:** Always use pre-increment for incrementing

This prevents a copy from being made, and that can hurt performance, especially when used with iterators

## **9:** Use switches wherever possible

Performance

## **10:** Use std::vectors wherever possible

Data locality massively increases performance
