#include "wormhole/Window.hpp"

namespace wh {
	Window::Window(const int width, const int height, const int refreshRate, const WindowMode displayMode, const std::string& title) : 
	m_title(title), m_width(width), m_height(height), m_refreshRate(refreshRate), m_displayMode(displayMode) {
		GLFWmonitor* monitor;

		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		if (displayMode == WindowMode::Windowed) {
			monitor = nullptr;
			glfwWindowHint(GLFW_REFRESH_RATE, refreshRate);
		}
		else {
			monitor = glfwGetPrimaryMonitor();
			const GLFWvidmode* mode = glfwGetVideoMode(monitor);

			if (displayMode == WindowMode::Borderless) {
				//borderless window mode can only be the dimensions used by the monitor

				m_width = mode->width;
				m_height = mode->height;
				m_refreshRate = mode->refreshRate;

				glfwWindowHint(GLFW_RED_BITS, mode->redBits);
				glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
				glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
				glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
			}
			else {
				glfwWindowHint(GLFW_REFRESH_RATE, refreshRate);
			}
		}

		m_handle = glfwCreateWindow(width, height, title.c_str(), monitor, 0);

		if (m_handle == nullptr) {
			throw std::runtime_error("wh::Window: Could not create window.");
		}
	}

	Window::Window(Window&& rhs) {
		m_handle = rhs.m_handle;
		m_title = rhs.m_title;
		m_width = rhs.m_width;
		m_height =  rhs.m_height;
		m_refreshRate = rhs.m_refreshRate;
		m_displayMode = rhs.m_displayMode;
		rhs.m_handle = nullptr;
	}

	Window::~Window() {
		glfwDestroyWindow(m_handle);
		m_handle = nullptr;
	}

	const Window& Window::operator=(Window&& rhs) {
		m_handle = rhs.m_handle;
		m_title = rhs.m_title;
		m_width = rhs.m_width;
		m_height = rhs.m_height;
		m_refreshRate = rhs.m_refreshRate;
		m_displayMode = rhs.m_displayMode;
		rhs.m_handle = nullptr;
		return *this;
	}

	void Window::changeTitle(const std::string& title) {
		glfwSetWindowTitle(m_handle, title.c_str());
		m_title = title;
	}

	void Window::changeDimensions(const int width, const int height) {
		glfwSetWindowSize(m_handle, width, height);
		m_width = width;
		m_height = height;
	}

	void Window::changeRefreshRate(const int refreshRate) {
		if (m_displayMode != WindowMode::Borderless) {
			if (m_displayMode == WindowMode::Windowed) {
				glfwSetWindowMonitor(m_handle, nullptr, 0, 0, m_width, m_height, refreshRate);
			}
			else if (m_displayMode == WindowMode::Fullscreen) {
				GLFWmonitor* monitor = glfwGetPrimaryMonitor();
				glfwSetWindowMonitor(m_handle, monitor, 0, 0, m_width, m_height, refreshRate);
			}

			m_refreshRate = refreshRate;
		}
	}

	void Window::changeDisplayMode(const WindowMode displayMode) {
		if (displayMode == WindowMode::Windowed) {
			glfwSetWindowMonitor(m_handle, nullptr, 0, 0, m_width, m_height, m_refreshRate);
		}
		else {
			GLFWmonitor* monitor = glfwGetPrimaryMonitor();

			if (displayMode == WindowMode::Borderless) {
				const GLFWvidmode* mode = glfwGetVideoMode(monitor);

				//if the window is currently fullscreen, we need to window to get the native monitor info
				if (m_displayMode == WindowMode::Fullscreen) {
					glfwSetWindowMonitor(m_handle, nullptr, 0, 0, m_width, m_height, m_refreshRate);
				}

				m_width = mode->width;
				m_height = mode->height;
				m_refreshRate = mode->refreshRate;
			}

			glfwSetWindowMonitor(m_handle, monitor, 0, 0, m_width, m_height, m_refreshRate);
		}
	}
}
