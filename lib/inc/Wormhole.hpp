#pragma once

#include "wormhole/Container.hpp"
#include "wormhole/Core.hpp"
#include "wormhole/EngineSystem.hpp"
#include "wormhole/File.hpp"
#include "wormhole/Memory.hpp"
#include "wormhole/RenderSystem.hpp"
#include "wormhole/ResourceSystem.hpp"
#include "wormhole/Utility.hpp"
#include "wormhole/Window.hpp"

#include "wormhole/EntryPoint.hpp"
