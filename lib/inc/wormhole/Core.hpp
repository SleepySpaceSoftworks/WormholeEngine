#pragma once

//std lib headers
#include <array>
#include <atomic>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <exception>
#include <functional>
#include <fstream>
#include <future>
#include <iostream>
#include <map>
#include <mutex>
#include <optional>
#include <set>
#include <thread>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

//platform header
#include "wormhole/Platform.hpp"

//vulkan header
#include "vulkan/vulkan.hpp"

//glfw header
#include "GLFW/glfw3.h"

//get configuration options
#include "wormhole/Config.hpp"

#if defined(NDEBUG)
	#define ASSERT(expr)
	#define DEBUGVAR(var)
#else
	#define ASSERT(expr) \
		if (expr) { } \
		else { \
				std::cerr << "Assertion of " << #expr << " failed in file " << __FILE__ << " at line " << __LINE__ ; \
				abort(); \
		}
	#define DEBUGVAR(var) std::cout << #var << ": " << var << std::endl;
#endif

//kilobyte
#define KB *1000
//megabyte
#define MB *1000000
//gigabyte
#define GB *1000000000

//compile-time check for 64-bit
static_assert(sizeof(size_t) == 8);

namespace wh {
	enum class Priority : char { Low, Medium, High };

	//forward declarations
	class Animation;
	class Audio;
	class DiskResource;
	class EngineSystem;
	class FileData;
	class Mesh;
	class Model;
	template <typename Ty> class Pool;
	class RenderSystem;
	class ResourceArchiver;
	class FileLoader;
	class ResourceSystem;
	template <typename Ty, std::size_t N> class RingBuffer;
	class Skeleton;
	class Stack;
	class String;
	class Texture;
	struct TextureImage;
	struct Vertex;
	class Video;
	class Window;

	//using declarations
	using UID = size_t;	
	using Byte = char;
}
