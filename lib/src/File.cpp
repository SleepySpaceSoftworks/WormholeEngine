#include "wormhole/File.hpp"

namespace wh {
	Pool<char> FileData::s_allocator(WH_FILEDATA_STACK_SIZE);

	FileData::FileData() {
		m_data = nullptr;
		m_size = 0;
	}

	FileData::FileData(const char* data, size_t len) : m_size(len) {
		m_data = s_allocator.allocate(m_size);

		if(m_data) {
			std::memcpy(m_data, data, m_size);
		}
		else {
			m_size = 0;
		}
	}

	FileData::FileData(std::ifstream& file) {
		file.seekg(0, std::ios::end);
		m_size = file.tellg();
		file.seekg(0, std::ios::beg);

    m_data = s_allocator.allocate(m_size);

		if(m_data) {
			file.read(m_data, m_size);
		}
		else {
			m_size = 0;
		}
	}

	FileData::FileData(FileData&& other) {
		this->m_data = other.m_data;
		this->m_pos = other.m_pos;
		this->m_size = other.m_size;

		other.m_data = nullptr;
		other.m_pos = 0;
		other.m_size = 0;
	}

	bool FileData::empty() const {
		return m_size == 0;
	}

	const char* FileData::data() const {
		return m_data;
	}

	FileData::~FileData() {
		s_allocator.deallocate(m_data, m_size);
	}

	FileData loadFile(const FilePath& path) noexcept {
		static std::mutex mutex;
		static FilePath cur_path;
		static std::ifstream cur_file;
		

		std::lock_guard lk(mutex);

		if(path != cur_path) {
			if(cur_file.is_open()) {
				cur_file.close();
			}

			cur_path = path;
			cur_file.open(cur_path.c_str(), std::ios::binary);

			if(!cur_file.is_open()) {
				return FileData();
			}
		}

		return FileData(cur_file);
	}
}
