#include "wormhole/ResourceSystem.hpp"

namespace wh {
	ResourceSystem::~ResourceSystem() {
		shutDown();
	}

	void ResourceSystem::startUp() {
		m_active.store(true);
		m_manager = std::thread{ &ResourceSystem::manage, this };
	}

	void ResourceSystem::shutDown() {
		m_active.store(false);
		//if manager is sleeping, we need to wake it so it can be joined
		m_condWakeManager.notify_all();

		if (m_manager.joinable()) {
			m_manager.join();
		}
	}
	
	void ResourceSystem::queueResource(DiskResource* resource, const Priority priority) {
		if (resource->state() == ResourceState::NotLoaded) {
			switch (priority) {
				case Priority::High:
					m_queueHigh.push(resource);
					break;
				case Priority::Medium:
					m_queueMedium.push(resource);
					break;
				case Priority::Low:
					m_queueLow.push(resource);
					break;
			}
			resource->m_state = ResourceState::Queued;

			//wake up manager since it has work now
			m_condWakeManager.notify_all();
		}
	}

	void ResourceSystem::demand(DiskResource* resource) {
		switch (resource->state()) {
			case ResourceState::NotLoaded:
				break;
			case ResourceState::Queued:
				break;
			case ResourceState::Loading:
				break;
			case ResourceState::Loaded:
				break;
		}
	}

	void ResourceSystem::manage() {
		while (m_active) {
			if (m_queueHigh.any()) {
				m_curResource.store(m_queueHigh.pop());
			}
			else if (m_queueMedium.any()) {
				m_curResource.store(m_queueMedium.pop());
			}
			else if (m_queueLow.any()) {
				m_curResource.store(m_queueLow.pop());
			}

			if (m_curResource != nullptr) {
				loadResource(m_curResource);

				m_condResourceLoaded.notify_all();
				m_curResource = nullptr;
			}

			//if all the queues are empty, let the thread get some much deserved rest
			if (!m_queueHigh.any() && !m_queueMedium.any() && !m_queueLow.any()) {
				std::unique_lock<std::mutex> lk( m_mutexWakeManager );
				m_condWakeManager.wait(lk);
			}
		}

	}

	void ResourceSystem::loadResource(DiskResource* resource) {
		resource->m_state = ResourceState::Loading;
		FileData data = loadFile(resource->path());
		resource->deserialize(data);
		resource->m_state = ResourceState::Loaded;
	}
}
