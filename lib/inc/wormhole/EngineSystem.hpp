#pragma once
#include "wormhole/Core.hpp"

namespace wh {
	class EngineSystem {
	public:
		virtual ~EngineSystem() { }

		virtual void startUp() = 0;
		virtual void shutDown() = 0;
	};
}