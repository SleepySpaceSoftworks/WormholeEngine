#pragma once 
#include "wormhole/Core.hpp"

namespace wh {

	enum class WindowMode : char { Windowed, Borderless, Fullscreen };

	/*
	A GLFW window.
	*/
	class Window {
	public:
		Window(const int width, const int height, const int refreshRate, const WindowMode displayMode, const std::string& title);
		Window(const Window&) = delete;
		Window(Window&& rhs);
		~Window();

		const Window& operator=(Window&& rhs);

		inline std::string title() const noexcept { return m_title; }
		inline GLFWwindow* handle() const noexcept { return m_handle; }
		inline int width() const noexcept { return m_width; }
		inline int height() const noexcept { return m_height; }
		inline int refreshRate() const noexcept { return m_refreshRate; }
		inline WindowMode displayMode() const noexcept { return m_displayMode; }

		void changeTitle(const std::string& title);
		void changeDimensions(const int width, const int height);
		void changeRefreshRate(const int refreshRate);
		void changeDisplayMode(const WindowMode displayMode);
	private:
		GLFWwindow* m_handle{ nullptr };

		std::string m_title;
		int m_width;
		int m_height;
		int m_refreshRate;
		WindowMode m_displayMode;
	};
}
