#pragma once
#include "wormhole/Core.hpp"

namespace wh {
	/*
		A thread-safe stack allocator
	*/
	class Stack {
	public:
		Stack() = delete;
		explicit Stack(const size_t size);
		Stack(const Stack&) = delete;
		Stack(Stack&&) = delete;
		~Stack();

		[[nodiscard]] void* allocate(const size_t size, const size_t alignment);
		[[nodiscard]] void* allocate(const size_t size);


		void deallocate(void* p, const size_t size, const size_t alignment);
		void deallocate(void* p, const size_t);

		size_t size() const { return m_size; }
		size_t capacity() const { return m_capacity; }
	private:
		struct FreeBlockHead {
			size_t size; //size of the block
			size_t next; //offset from _data for the next head
		};

		inline FreeBlockHead* getBlock(const size_t offset) {
			return reinterpret_cast<FreeBlockHead*>(reinterpret_cast<Byte*>(m_data) + offset);
		}
		inline size_t getBlockOffset(FreeBlockHead* block) {
			return static_cast<size_t>(reinterpret_cast<Byte*>(block) - reinterpret_cast<Byte*>(m_data));
		}

		void* m_data;
		std::mutex m_dataMutex;
		const size_t m_capacity;
		std::atomic<size_t> m_freeBlockOffset{ 0 };
		std::atomic<size_t> m_size{ 0 };
	};

	template <typename Ty>
	class Pool {
	public:
		using value_type = Ty;
		using size_type = size_t;

		Pool() = delete;
		Pool(size_t size);
		Pool(const Pool&) = delete;
		Pool(Pool&& rhs) = delete;
		~Pool();

		[[nodiscard]] Ty* allocate(size_t size);
		void deallocate(Ty* p, size_t size);

		size_t size() const { return m_size; }
		size_t capacity() const { return m_capacity; }
	private:
		struct FreeBlockHead {
			size_t size; // size of the block
			size_t next; // offset from m_data for the next head
		};

		inline FreeBlockHead* getBlock(const size_t offset) {
			return reinterpret_cast<FreeBlockHead*>(m_data + offset);
		}
		inline size_t getBlockOffset(FreeBlockHead* block) {
			return static_cast<size_t>(reinterpret_cast<Ty*>(block) - m_data);
		}

		inline constexpr size_t static min_alloc() {
			return static_cast<size_t>(std::ceil(static_cast<float>(sizeof(FreeBlockHead)) / sizeof(Ty)));
		}

		std::mutex m_dataMutex;
		Ty* m_data;
		const size_t m_capacity;
		std::atomic<size_t> m_freeBlockOffset{ 0 };
		std::atomic<size_t> m_size{ 0 };
	};

	template <typename Ty>
	Pool<Ty>::Pool(size_t size) : m_data{ reinterpret_cast<Ty*>(alignedAlloc(size * sizeof(Ty), alignof(Ty))) }, m_capacity{ size } {
		auto node = getBlock(0);
		node->size = size;
		node->next = 0;
	}

	template <typename Ty>
	Pool<Ty>::~Pool() {
		std::lock_guard lk(m_dataMutex);
		alignedFree(m_data);
	}

	template<typename Ty>
	[[nodiscard]] Ty* Pool<Ty>::allocate(size_t size) {
		if((sizeof(Ty) * size) < sizeof(FreeBlockHead)) {
			size = min_alloc();
		}
		
		if (m_capacity - m_size < size) {
			throw std::bad_alloc();
		}

		std::lock_guard lk(m_dataMutex);

		// find the first free node that can fit the request
		FreeBlockHead* previousBlock = nullptr;
		FreeBlockHead* currentBlock = getBlock(m_freeBlockOffset);

		while (currentBlock->next != 0 && currentBlock->size < size) {
			previousBlock = currentBlock;
			currentBlock = getBlock(currentBlock->next);
		}

		if (currentBlock->size < size) {
			throw std::bad_alloc();
		}

		// if the block is the exact size we need
		if (currentBlock->size == size) { 
			// if currentBlock is not the last block
			if (currentBlock->next != 0) {
				// if currentBlock is the first block
				if (previousBlock == nullptr) {
					if (currentBlock->next != 0) {
						m_freeBlockOffset = currentBlock->next;
					}
				}
				else {
					previousBlock->next = currentBlock->next;
				}
			}
			// if currentBlock is the last block
			else {
				auto newBlock = getBlock(getBlockOffset(currentBlock) + size);

				newBlock->size = currentBlock->size - size;
				newBlock->next = 0;

				if (previousBlock == nullptr) {
					m_freeBlockOffset = getBlockOffset(newBlock);
				}
				else {
					previousBlock->next = getBlockOffset(newBlock);
				}
			}
		}
		// the block size is bigger than what we need
		else { 
			// make a new sub-block
			auto newBlock = getBlock(getBlockOffset(currentBlock) + size);

			newBlock->size = currentBlock->size - size;
			newBlock->next = currentBlock->next;

			if (previousBlock == nullptr) {
				m_freeBlockOffset = getBlockOffset(newBlock);
			}
			else {
				previousBlock->next = getBlockOffset(newBlock);
			}
		}
	#ifndef NDEBUG
		// zero out the data in debug mode
		memset(currentBlock, 0, size * sizeof(Ty));
	#endif
		m_size += size;
		return reinterpret_cast<Ty*>(currentBlock);
	}

	template<typename Ty> 
	void Pool<Ty>::deallocate(Ty* p, size_t size) {
		if (p == nullptr) {
			return;
		}

		if((sizeof(Ty) * size) < sizeof(FreeBlockHead)) {
			size = min_alloc();
		}

		std::lock_guard lk(m_dataMutex);

		// assert that the pointer is within the pool's data
		ASSERT(m_data <= p);
		ASSERT(p <= m_data + m_capacity);

		if (m_size != m_capacity) {
			FreeBlockHead* currentBlock = getBlock(m_freeBlockOffset);

			// try to have currentBlock be the block just behind p
			while (currentBlock->next != 0 && currentBlock->next < static_cast<size_t>(p - m_data)) {
				currentBlock = getBlock(currentBlock->next);
			}

			// if currentBlock is in front of p
			if (p  < reinterpret_cast<Ty*>(currentBlock)) { 
				FreeBlockHead* newBlock = reinterpret_cast<FreeBlockHead*>(p);

				// if the blocks are back to back
				if (p + size == reinterpret_cast<Ty*>(currentBlock)) { 
					newBlock->size = size + currentBlock->size;
					newBlock->next = currentBlock->next;
				}
				else {
					newBlock->size = size;
					newBlock->next = getBlockOffset(currentBlock);
				}

				m_freeBlockOffset = getBlockOffset(newBlock);
			}
			// current block is behind p
			else { 
				// if the blocks are back to back
				if (p == reinterpret_cast<Ty*>(currentBlock) + currentBlock->size) { 
					currentBlock->size += size;
					if (currentBlock->next != 0) {
						auto nextBlock = getBlock(currentBlock->next);

						if (reinterpret_cast<Ty*>(currentBlock) + currentBlock->size == reinterpret_cast<Ty*>(nextBlock)) {
							currentBlock->size += nextBlock->size;
							currentBlock->next = nextBlock->next;
						}
					}
				}
				else {
					FreeBlockHead* newBlock = reinterpret_cast<FreeBlockHead*>(p);
					newBlock->size = size;
					newBlock->next = currentBlock->next;
					currentBlock->next = getBlockOffset(newBlock);
				}
			}
		}
		else {
			FreeBlockHead* newBlock = reinterpret_cast<FreeBlockHead*>(p);
			newBlock->size = size;
			newBlock->next = 0;
			m_freeBlockOffset = getBlockOffset(newBlock);
		}

		m_size -= size;
	}
}
