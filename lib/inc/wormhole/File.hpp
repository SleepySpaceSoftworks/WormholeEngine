#pragma once
#include "wormhole/Core.hpp"
#include "wormhole/Utility.hpp"

#include "wormhole/Memory.hpp"

namespace wh {
	typedef String FilePath;

#ifndef WH_FILEDATA_STACK_SIZE
#define WH_FILEDATA_STACK_SIZE 50 KB
#endif

	class FileData {
		public:
		FileData();
		FileData(const char* data, size_t len);
		explicit FileData(std::ifstream& file);
		FileData(const FileData&) = delete;
		FileData(FileData&&);
		~FileData();

		bool empty() const;
		const char* data() const;

		template<typename Ty> void read(Ty* dest, size_t count);

		private:
		static Pool<char> s_allocator;

		char* m_data;
		size_t m_size;
		size_t m_pos{ 0 }; // current offset
	};

	template<typename Ty> void FileData::read(Ty* dest, size_t count) {
		std::memcpy(dest, m_data + m_pos, sizeof(Ty) * count);
		m_pos += sizeof(Ty) * count;
	}

	FileData loadFile(const FilePath& path) noexcept;
}
