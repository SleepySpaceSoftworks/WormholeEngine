#pragma once
#include "wormhole/Core.hpp"

#include "wormhole/Container.hpp"
#include "wormhole/File.hpp"
#include "wormhole/EngineSystem.hpp"
#include "wormhole/Memory.hpp"

namespace wh {
	enum class ResourceState : char { NotLoaded, Queued, Loading, Loaded };

	class DiskResource {
	public:
		DiskResource(std::string name, const FilePath& onDiskLocation) : m_name(name), m_path(onDiskLocation) { }
		~DiskResource() { }

		FilePath path() const noexcept { return m_path; }
		std::string name() const noexcept { return m_name; }
		ResourceState state() const noexcept { return m_state; }

		virtual void deserialize(FileData& data) = 0;
		
		friend class ResourceSystem;
	private:
		std::string m_name;
		FilePath m_path;
		std::atomic<ResourceState> m_state = ResourceState::NotLoaded;
	};

	/*
		Class that manages the loading of resources
	*/
	class ResourceSystem final : public EngineSystem {
	public:
		ResourceSystem() = default;
		ResourceSystem(const ResourceSystem&) = delete;
		ResourceSystem(ResourceSystem&&) = delete;
		ResourceSystem& operator=(const ResourceSystem&) = delete;
		ResourceSystem& operator=(ResourceSystem&&) = delete;
		~ResourceSystem();

		void startUp() override;
		void shutDown() override;
		
		/*
			Adds the resource to a loading queue based off of priority
		*/
		void queueResource(DiskResource* resource, const Priority priority);

		/*
			Pauses the calling thread's execution to load in the resource
			Nothing happens if the resource is already loaded
		*/
		void demand(DiskResource* resource);
	private:
		void manage();
		void loadResource(DiskResource* resource);
		
		static constexpr std::size_t s_queueSize = 512;
		
		RingBuffer<DiskResource*, s_queueSize> m_queueHigh; //high-priority loading queue
		RingBuffer<DiskResource*, s_queueSize> m_queueMedium; //medium-priority loading queue
		RingBuffer<DiskResource*, s_queueSize> m_queueLow; //low-priority loading queue

		std::mutex m_mutexWakeManager;
		std::condition_variable m_condWakeManager; //signals when manager has work

		std::atomic<DiskResource*> m_curResource{ nullptr };
		std::mutex m_mutexResourceLoaded;
		std::condition_variable m_condResourceLoaded;

		std::thread m_manager;
		std::atomic<bool> m_active{ false };
	};
}
