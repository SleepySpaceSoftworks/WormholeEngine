#pragma once
#include <wormhole/Core.hpp>

namespace wh {
    class Application {
    public:
        Application() { }
        virtual ~Application() { }

        virtual void run() = 0;
    };
}