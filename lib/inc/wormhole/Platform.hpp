#pragma once
#include <cstring>
#include <cstdint>

//platform specific headers
#if defined _WINDOWS
#include <windows.h>
#include <WinBase.h>
#elif defined __linux__
#include <dlfcn.h>
#include <errno.h>
#include <sys/mman.h>
#endif

namespace wh {
	//platform specific aliases
#if defined _WINDOWS
	using SharedLibrary = HINSTANCE;
#elif defined __linux__
	using SharedLibrary = void*;
#endif

	SharedLibrary openSharedLibrary(const char* path);
	void* getProcAddress(SharedLibrary lib, const char* procName);
	void closeSharedLibrary(SharedLibrary lib);

	void* alignedAlloc(size_t size, size_t alignment);
	void alignedFree(void* p);
}
